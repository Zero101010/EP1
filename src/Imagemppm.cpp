#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include "imagemppm.hpp"

using namespace std;

// Implementado o método construtor padrão.
ImagemPPM::ImagemPPM() {
	imagem = "";
	numeromagico = "";
	cor = 0;
	tamanho_total = 0;
	tamanho_imagem = 0;
	linha = 0;
	coluna = 0;
	tamanho_cabecalho = 0;

}
// Implementando o método destrutor.
ImagemPPM::~ImagemPPM() {

}
// Implementando os métodos acessores.
string ImagemPPM::getImagem() {
	return imagem;
}
void ImagemPPM::setImagem(string imagem) {
	this-> imagem = imagem;
}
string ImagemPPM::getNumeroMagico() {
        return numeromagico;
}
void ImagemPPM::setNumeroMagico(string numeromagico) {
        this-> numeromagico = numeromagico;
}
unsigned char ImagemPPM::getCor() {
        return cor;
}
void ImagemPPM::setCor(unsigned char cor) {
        this-> cor = cor;
}
int ImagemPPM::getTamanhoTotal() {
        return tamanho_total;
}
void ImagemPPM::setTamanhoTotal(int tamanho_total) {
        this-> tamanho_total = tamanho_total;
}
int ImagemPPM::getTamanhoImagem() {
        return tamanho_imagem;
}
void ImagemPPM::setTamanhoImagem(int tamanho_imagem) {
        this-> tamanho_imagem = tamanho_imagem;
}
int ImagemPPM::getLinha() {
        return linha;
}
void ImagemPPM::setLinha(int linha) {
        this-> linha = linha;
}
int ImagemPPM::getColuna() {
        return coluna;
}
void ImagemPPM::setColuna(int coluna) {
        this-> coluna = coluna;
}
void ImagemPPM::setTamanhoCabecalho(int tamanho_cabecalho) {
        this-> tamanho_cabecalho = tamanho_cabecalho;
}
int ImagemPPM::getTamanhoCabecalho() {
        return tamanho_cabecalho;
}
// Implementando a função que abre a imagem e guarda todas as informações em forma de caracteres.
void ImagemPPM::AbrirImagem() {
	string imagem = "";
	string endereco = "";
	int contador = 0;
	
	cout << "Insira o endereço da imagem a ser filtrada: ";
	cin >> endereco;
	cout << endl;
 	
	ifstream inFile;
	inFile.open(endereco.c_str());
	
	if (inFile.fail()) {
	cerr << "O arquivo não foi encontrado" << endl;
	exit(1);
	}

        while (!inFile.eof()) {
                imagem+= inFile.get();
                contador++;

        }
  
	this-> imagem = imagem;
	tamanho_total = contador;

}
// Implementando a função de extrair os dados do cabeçalho, como: Número mágico e Nível máximo de cor.
void ImagemPPM::ExtrairAtributos() {
	string numeromagico;
	string comentario;
	string linha;
	string coluna;
	string cor;
	int contador = 0;
	int tamanho_imagem = 0;

	for (int i = 0; i < tamanho_total; i++) {
		if (imagem[i] == '\n') {
			contador++;
		}
		if ((imagem[i] != '\n') && (contador == 0)) {
			numeromagico += imagem[i];
		} else
		
		 if (imagem[i] == '#') {
			comentario += imagem[i];
			contador = 1;
		}else

		if ((contador == 2) && (imagem[i] != ' ')) {
			linha += imagem[i];
		} else

		if ((contador == 2) && (imagem[i] == ' ')) {
			contador ++;
		} else

		if (contador == 3) {
                        coluna += imagem[i];
                }else 

		if (contador == 4) {
			cor += imagem[i];
		} else
	
		if (imagem[i] == 5) {
			break;
		}
	
	}

	tamanho_imagem = atoi(linha.c_str())*atoi(coluna.c_str());
	this-> numeromagico = numeromagico;
	this-> linha = atoi(linha.c_str());
    this-> coluna = atoi(coluna.c_str());
	this-> tamanho_imagem = tamanho_imagem;
    this-> cor = atoi(cor.c_str());   
}
// Implementando a função conta o tamanho do cabeçalho.
void ImagemPPM::ExtrairCabecalho() {
	int cabecalho = 0;
	int contador = 0;
	string comentario;

	for(int i=0; i < tamanho_total; i++) {
		if (imagem[i] == '\n') {
			contador++;
		}

		if (imagem[i] == '#') {
			comentario += imagem[i];
			contador = 1;
		} else 
		
		if (contador == 4) {
			break;
		}
		cabecalho++; 		
	}

	tamanho_cabecalho = cabecalho;
}
