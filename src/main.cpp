#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "imagemppm.hpp"
#include "filtros.hpp"
#include "filtroNegativo.hpp"
#include "filtropolarizado.hpp"
#include "filtropretoebranco.hpp"


using namespace std;

int main(int argc, char ** argv) {
int valor;

cout << "Escolha:\n 1)para aplicar o filtro negativo\n 2)para aplicar o filtro polarizado\n 3) para aplicar o filtro preto e branco\n 4)para aplicar o filtro de media: ";
cin >> valor;
cout << endl;

// Criando o objeto.
try {
    if (valor == 1) {
        FiltroNegativo * imagem = new FiltroNegativo();
        // Abrindo a imagem, extraindo os atributos, extraindo o cabeçalho e aplicando o filtro.
        imagem->AbrirImagem();
        imagem->ExtrairAtributos();
        imagem->ExtrairCabecalho();

        if (imagem->getNumeroMagico() != "P6") {
            cout << "Tipo de imagem inválido!" << endl;
            exit(1); 
        }
        else {
            imagem->AplicaFiltro();
            cout<<"-------------------MENU---------------------------"<<endl;
            cout << "Numero mágico: " << imagem->getNumeroMagico() << endl;
            cout << "Largura: " << imagem->getLinha() << endl;
            cout << "Altura: " << imagem->getColuna() << endl;
            cout << "Nível máximo de cor:" << ' ';
            printf("%d \n", imagem->getCor());
            cout<<"--------------------------------------------------"<<endl;
           
        }       
    } else 

    if (valor == 2) {
        FiltroPolarizado * imagem = new FiltroPolarizado();
        // Abrindo a imagem, extraindo os atributos, extraindo o cabeçalho e aplicando o filtro.
        imagem->AbrirImagem();
        imagem->ExtrairAtributos();
        imagem->ExtrairCabecalho();

        if (imagem->getNumeroMagico() != "P6") {
            cout << "Tipo de imagem inválido!" << endl;
            exit(1); 
        }
        else {
            imagem->AplicaFiltro();
            cout<<"-------------------MENU---------------------------"<<endl;
            cout << "Numero mágico: " << imagem->getNumeroMagico() << endl;
            cout << "Largura: " << imagem->getLinha() << endl;
            cout << "Altura: " << imagem->getColuna() << endl;
            cout << "Nível máximo de cor:" << ' ';
            printf("%d \n", imagem->getCor());
            cout<<"--------------------------------------------------"<<endl;
            
        }       
    } else

     if (valor == 3) {
        FiltroPretoeBranco * imagem = new FiltroPretoeBranco();
        // Abrindo a imagem, extraindo os atributos, extraindo o cabeçalho e aplicando o filtro.
        imagem->AbrirImagem();
        imagem->ExtrairAtributos();
        imagem->ExtrairCabecalho();

        if (imagem->getNumeroMagico() != "P6") {
            cout << "Tipo de imagem inválido!" << endl;
            exit(1); 
        }
        else {
            imagem->AplicaFiltro();
            cout<<"-------------------MENU---------------------------"<<endl;
            cout << "Numero mágico: " << imagem->getNumeroMagico() << endl;
            cout << "Largura: " << imagem->getLinha() << endl;
            cout << "Altura: " << imagem->getColuna() << endl;
            cout << "Nível máximo de cor:" << ' ';
            printf("%d \n", imagem->getCor());
            cout<<"--------------------------------------------------"<<endl;
            
                    
        }       
     } /*else

    if (valor == 4) {    
        FiltroMedia * imagem = new FiltroMedia();
        // Abrindo a imagem, extraindo os atributos, extraindo o cabeçalho e aplicando o filtro.
        imagem->AbrirImagem();
        imagem->ExtrairAtributos();
        imagem->ExtrairCabecalho();

        if (imagem->getNumeroMagico() != "P6") {
            cout << "Tipo de imagem inválido!" << endl;
            exit(1); 
        }
        else {
            imagem->AplicaFiltro();

            cout << "Numero mágico: " << imagem->getNumeroMagico() << endl;
            cout << "Largura: " << imagem->getLinha() << endl;
            cout << "Altura: " << imagem->getColuna() << endl;
            cout << "Nível máximo de cor:" << ' ';
            printf("%d \n", imagem->getCor());

                    }   
    } */ else
    if ((valor != 1) || (valor != 2) || (valor != 3) || (valor != 4)) {
        throw(6);
    } 
}
catch (int captura) {
    cout << "Escolha inválida";
    cout << endl;

}

return 0;

}
