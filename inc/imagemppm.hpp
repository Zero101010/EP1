#ifndef IMAGEMPPM_HPP
#define IMAGEMPPM_HPP
#include <iostream>
#include <string>

using namespace std;

// Criando a classe principal.
class ImagemPPM {
        private:
        string imagem;
        string numeromagico;
        unsigned char cor;
        int tamanho_total;
        int tamanho_imagem;
        int linha;
        int coluna;
        int tamanho_cabecalho;
        public:
        ImagemPPM(); // Construtor padrão.
        ~ImagemPPM(); // Destrutor.
        // Métodos acessores.
        string getImagem();
        void setImagem(string imagem);
        string getNumeroMagico();
        void setNumeroMagico(string numeromagico);
        unsigned char getCor();
        void setCor(unsigned char nivel_cor);
        int getTamanhoTotal();
        void setTamanhoTotal(int tamanho_total);
        int getTamanhoImagem();
        void setTamanhoImagem(int tamanho_imagem);
        int getLinha();
        void setLinha(int linha);
        int getColuna();
        void setColuna(int coluna);
        int getTamanhoCabecalho();
        void setTamanhoCabecalho(int tamanho_cabecalho);
        // Funções de abrir a imagem e extrair os dados do cabeçalho.
        void AbrirImagem();
        void ExtrairAtributos();
        void ExtrairCabecalho();

};

#endif
