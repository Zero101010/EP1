#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP

#include <iostream>
#include <string>
#include "filtros.hpp"

using namespace std;

// Criando a classe filha NovaImagem.
class FiltroNegativo : public Filtros {
	public:
	FiltroNegativo(); // Construtor padrão.
	~FiltroNegativo(); // Destrutor.
	// Função de aplicar o filtro.
	void AplicaFiltro();
};

#endif